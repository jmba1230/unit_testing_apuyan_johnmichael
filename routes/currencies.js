const express = require("express")
const router = express.Router();
const { exchangeRates } = require("../src/utils.js");

// GET ALL
router.get('/', (req,res)=>{
    return res.send({"currencies": Object.keys(exchangeRates)})
})

module.exports = router;