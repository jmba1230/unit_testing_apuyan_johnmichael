// IMPORT DEPENDENCIES
const chai = require ("chai");
const { expect } = chai;
const chaiHttp=require("chai-http");
chai.use(chaiHttp);

// SERVER URL
const serverUrl = "http://localhost:3000";

// TEST SUITE
describe('GET ALL RATES',()=>{
    it("should accept http requests", (done)=>{
        chai.request(serverUrl)
        .get('/rates')
        .end((err,res)=>expect(res).to.not.equal(undefined))
        done()
    })

    it("should have a status of 200", (done)=>{
        chai
        .request(serverUrl)
        .get("/rates")
        .end((err,res)=>{
            expect(res).to.have.status(200)
            done()
        })
    })

    it("should return an object w/ a property of rates", (done)=>{
        chai
        .request(serverUrl)
        .get("/rates")
        .end((err,res)=>{
            expect(res.body).to.have.property('rates')
            done()
        })
    })

    it("should have an object w/ a property of rates w/ the length of 5", (done)=>{
        chai
        .request(serverUrl)
        .get("/rates")
        .end((err,res)=>{
            expect(Object.keys(res.body.rates)).lengthOf(5)
            done()
        })
    })

})


// TEST SUIT CURRENCIES
describe('GET ALL CURRENCIES',()=>{
    it("should accept http requests", (done)=>{
        chai.request(serverUrl)
        .get('/currencies')
        .end((err,res)=>expect(res).to.not.equal(undefined))
        done()
    })

    it("should have a status of 200", (done)=>{
        chai
        .request(serverUrl)
        .get("/currencies")
        .end((err,res)=>{
            expect(res).to.have.status(200)
            done()
        })
    })

    it("should have a property of currencies", (done)=>{
        chai
        .request(serverUrl)
        .get("/currencies")
        .end((err,res)=>{
            expect(res.body).to.have.property('currencies')
            done()
        })
    })

    it("should have a property of currencies with a length of 5", (done)=>{
        chai
        .request(serverUrl)
        .get("/currencies")
        .end((err,res)=>{
            expect(Object.keys(res.body.currencies)).lengthOf(5)
            done()
        })
    })

    it("should have a property of currencies that is an array", (done)=>{
        chai
        .request(serverUrl)
        .get("/currencies")
        .end((err,res)=>{
            expect(Object.keys(res.body.currencies)).instanceOf(Array)
            done()
        })
    })

})